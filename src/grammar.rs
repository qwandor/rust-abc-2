use datatypes::*;

peg::parser! {
/// Module generated by [rust_peg](https://crates.io/crates/peg)
///
/// Each function corresponds to a rule in the grammar and can be called with an input string
/// to begin parsing that type of ABC object.
///
/// # Examples
/// Usually you will want to parse whole ABC files in which case you will want to use
/// `abc::tune_book`.
/// ```
/// use abc_parser::datatypes::*;
/// use abc_parser::abc;
///
/// let parsed = abc::tune_book("X:1\nT:Example\nK:D\n").unwrap();
/// assert_eq!(
///     parsed,
///     TuneBook::new(None, vec![
///         Tune::new(
///             TuneHeader::new(vec![
///                 InfoField::new('X', "1".to_string()),
///                 InfoField::new('T', "Example".to_string()),
///                 InfoField::new('K', "D".to_string())
///             ]),
///             None
///         )
///     ])
/// )
/// ```
/// If you know that you will only be parsing one tune you can use `abc::tune`.
/// ```
/// use abc_parser::datatypes::*;
/// use abc_parser::abc;
///
/// let parsed = abc::tune("X:1\nT:Example\nK:D\n").unwrap();
/// assert_eq!(
///     parsed,
///     Tune::new(
///         TuneHeader::new(vec![
///             InfoField::new('X', "1".to_string()),
///             InfoField::new('T', "Example".to_string()),
///             InfoField::new('K', "D".to_string())
///         ]),
///         None
///     )
/// )
/// ```
pub grammar abc() for str {
    rule number() -> u32
        = n:$(['1'..='9']['0'..='9']*) { n.parse().unwrap() }

    rule space()
        = [' ' | '\t']+

    rule optional_space()
        = [' ' | '\t']*

    pub rule tune_book() -> TuneBook
        = h:file_header()? ts:tune()* { TuneBook::new(h, ts) } /
          ![_] { TuneBook::new(None, vec![]) }

    pub rule file_header() -> FileHeader
        = fields:info_field_any()* "\n" { FileHeader::new(fields) }

    pub rule tune() -> Tune
        = h:tune_header() b:tune_body()? { Tune::new(h, b) }

    pub rule tune_header() -> TuneHeader
        = "X:" optional_space() n:number() "\n"
          t:info_field(<$("T")>)
          extra:info_field_non_special()*
          k:info_field(<$("K")>) {
            let mut info = vec![InfoField::new('X', n.to_string()), t];
            for field in extra.into_iter() { info.push(field); }
            info.push(k);
            TuneHeader::new(info)
        }

    rule info_field<'a>(n: rule<&'a str>) -> InfoField
        = name:n() ":" optional_space() t:$((!"\n"[_])*) "\n" {
        InfoField::new(
            name.chars().next().unwrap(),
            t.to_string()
        )
    }

    rule info_field_any() -> InfoField
        = info_field(<$(['A'..='Z' | 'a'..='z'])>)

    rule info_field_non_special() -> InfoField
        = info_field(<$(!['K' | 'X']['A'..='Z' | 'a'..='z'])>)

    pub rule tune_body() -> TuneBody
        = m:music_line()+ { TuneBody::new(m) } /
          "\n" { TuneBody::new(vec![]) }

    pub rule music_line() -> MusicLine
        = s:music_symbol()+ music_line_end() { MusicLine::new(s) }

    pub rule music_symbol() -> MusicSymbol
        = n:note() "`"* { n } /
          r:rest() { r } /
          c:chord() { c } /
          b:bar() { b } /
          e:ending() { e } /
          g:grace_notes() { g } /
          t:tuplet() { t } /
          space() { MusicSymbol::VisualBreak }

    pub rule note() -> MusicSymbol
        = d:decorations() a:accidental()? n:note_uppercase() o:octave()? l:length()? t:tie()? {
            MusicSymbol::new_note(
                d, a, n, o.unwrap_or(1), l.unwrap_or(1.0), t
            )
        } /
        d:decorations() a:accidental()? n:note_lowercase() o:octave()? l:length()? t:tie()? {
            MusicSymbol::new_note(
                d, a, n, o.unwrap_or(1) + 1, l.unwrap_or(1.0), t
            )
        }

    pub rule note_uppercase() -> Note
        = "A" { Note::A } /
          "B" { Note::B } /
          "C" { Note::C } /
          "D" { Note::D } /
          "E" { Note::E } /
          "F" { Note::F } /
          "G" { Note::G }

    pub rule note_lowercase() -> Note
        = "a" { Note::A } /
          "b" { Note::B } /
          "c" { Note::C } /
          "d" { Note::D } /
          "e" { Note::E } /
          "f" { Note::F } /
          "g" { Note::G }

    rule tie() -> Tie
        = ".-" { Tie::Dotted } /
          "-" { Tie::Solid }

    pub rule bar() -> MusicSymbol
        = b:$([':' | '|' | '[' | '\\' | ']']+) { MusicSymbol::Bar(b.to_string()) }

    rule music_line_end()
        = "\n" / ![_]

    rule decorations() -> Vec<Decoration>
        = decoration()*

    rule decoration() -> Decoration
        = d:decoration_symbol() space()? { d }

    rule decoration_symbol() -> Decoration
        = "." { Decoration::Staccato } /
          "~" { Decoration::Roll } /
          "H" { Decoration::Fermata } /
          "L" { Decoration::Accent } /
          "M" { Decoration::LowerMordent } /
          "O" { Decoration::Coda } /
          "P" { Decoration::UpperMordent } /
          "S" { Decoration::Segno } /
          "T" { Decoration::Trill } /
          "u" { Decoration::UpBow } /
          "v" { Decoration::DownBow } /
          "!" d:$((!['\n' | '!'][_])+) "!" { Decoration::Unresolved(d.to_string()) }

    rule accidental() -> Accidental
        = "^^" { Accidental::DoubleSharp } /
          "__" { Accidental::DoubleFlat } /
          "^" { Accidental::Sharp } /
          "_" { Accidental::Flat } /
          "=" { Accidental::Natural }

    rule octave() -> i8
        = p:$([',' | '\'']+) {
            let mut octave = 1;
            for c in p.chars() {
                match c {
                    ',' => octave -= 1,
                    '\'' => octave += 1,
                    _ => panic!("Parser malfunctioned. Unexpected octave identifier")
                }
            }
            octave
        }

    rule length() -> f32
        = "/" n:number() { 1.0 / (n as f32) } /
          s:$("/"+) { 1.0 / (s.len() as f32).exp2() } /
          n1:number() "/" n2:number() { (n1 as f32) / (n2 as f32) } /
          n:number() { n as f32 }

    pub rule rest() -> MusicSymbol
        = "z" n:number()? { MusicSymbol::Rest(Rest::Note(n.unwrap_or(1))) } /
          "Z" n:number()? { MusicSymbol::Rest(Rest::Measure(n.unwrap_or(1))) } /
          "x" n:number()? { MusicSymbol::Rest(Rest::NoteHidden(n.unwrap_or(1))) } /
          "X" n:number()? { MusicSymbol::Rest(Rest::MeasureHidden(n.unwrap_or(1))) }

    pub rule ending() -> MusicSymbol
        = "[" n:number() space()? { MusicSymbol::Ending(n) }

    pub rule chord() -> MusicSymbol
        = d:decorations() "[" n:note()+ "]" l:length()? {
            MusicSymbol::Chord {
                decorations: d,
                notes: n,
                length: l.unwrap_or(1f32)
            }
        }

    pub rule grace_notes() -> MusicSymbol
        = "{" a:"/"? n:note()* "}" {
            MusicSymbol::GraceNotes {
                acciaccatura: a,
                notes: n
            }
        }

    pub rule tuplet() -> MusicSymbol
        = "(" p:number() space()? n:note()*<{ p as usize }> {?
              MusicSymbol::tuplet_with_defaults(p, None, None, n)
          } /
          "(" p:number() ":" q:number()? ":" r:number()? space()? n:note()*<{ p as usize }> {?
              MusicSymbol::tuplet_with_defaults(p, q, r, n)
          } /
          "(" p:number() ":" q:number() space()? n:note()*<{ p as usize }> {?
              MusicSymbol::tuplet_with_defaults(p, Some(q), None, n)
          }
}}
